var appServerUrl = null;

$(function() {
	createNav("config_nav", 1);
	setEl();
	time();

	getData();
	window.setInterval(function() {
		var width = window.innerWidth;
		var height = window.innerHeight;

		if (width != originWidth || height != originHeight) {
			location.reload();
		};
	}, 1000 * 10);

	chkBanner();
});

var tgVal = [];
function addTgCnt(){
	var dataList = [];
	var chk = true;
	
	$("#content_table tbody tr").each(function(idx, data){
		var obj = {
				dvcId : $(data).find("td:nth(0)").attr("dvcId")
				//,workIdx : $(data).find("td:nth(1)").attr("workIdx")
				,tgCnt : $(data).find("td:nth(1) input").val()
				//,cntPerCyl : $(data).find("td:nth(2) input").val()
				,tgRunTime : $(data).find("td:nth(2) input").val()
		}
		
		//console.log(obj.tgRunTime);
		if(obj.tgRunTime > 24){
			alert("목표 가동시간이 24시간 보다 클 수 없습니다.");
			chk = false;
			return;
		}
		
		dataList.push(obj)
	});
	
	if(chk == true){
		tgVal = {
				val : dataList
			}
			
			console.log(tgVal);
			
			var url = ctxPath + "/addTgCnt.do";
			var param = "val=" + JSON.stringify(tgVal) + 
						"&workDate=" + $("#today").val();
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType :"text",
				success : function(data){
					if(data=="success"){
						alert("저장되었습니다.");
						getData();
					}else{
						alert("저장되지 않았습니다!\n모든 값이 제대로 입력되었는지 확인하십시오!");
					}
				}
			});
	}
	
};

function getData(){
	var url = ctxPath + "/getTgCnt.do";
	var param = "workDate=" + $("#today").val();
	
	totLength = 0;
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success : function(data){
			
			var json = data.dataList;
			
			//totLength = json.length;
			
			var tr = "";
			var className = "tr1";
			//var workIdxStr;
			
			$(json).each(function(idx, data){
				
//				if(data.workIdx == 1){
//					workIdxStr = "<td style='text-align: center' workIdx='" + data.workIdx + "'> 야 </td>";
//				} else {
//					workIdxStr = "<td style='text-align: center' workIdx='" + data.workIdx + "'> 주 </td>";
//				}
				
				tr += "<Tr class='" + className + "'>" + 
						"<td style='text-align:center' dvcId='" + data.dvcId + "'>" +  data.name + "</td>" +
						//"<td><input type='text' value='" + data.workIdx + "'></td>" +
						//workIdxStr + 
						"<td><input type='text' style='ime-mode:disabled; text-align: center; font-weight: bold; font-size: getElSize(35);' onkeypress='checkForNumber();' onBlur='checkForNumber();'" +
						"value='" + data.tgCnt + "'></td>" +
//						"<td><input type='text' style='text-align: center' value='" + data.cntPerCyl + "'></td>" + 
						"<td><input type='text' style='ime-mode:disabled; text-align: center; font-size: getElSize(35);' onkeypress='checkForNumber();' onBlur='checkForNumber();'" +
						"value='" + data.tgRunTime + "'></td>" +
					"</tr>";
				
				if(data.tgCnt != ''){
					totLength += 1;
				}
				
				if(className=="tr1"){
					className = "tr2"
				}else{
					className = "tr1"
				}
			});
			
			$("#content_table tbody").html(tr)
			
			$("#content_table td").css({
				"color" : "white",
				"border": getElSize(5) + "px solid rgb(50,50,50)",
				"text-align" : "center",
				"font-size" : getElSize(35),
				"padding" : getElSize(20),
			});
			
			
			$("#content_table input").css({
				"font-size" : getElSize(40) + "px"
			});
			
			$("#content_table thead td").css({
				"background-color" : "black"
			})
			
			$(".tr1 td").css({
				"background-color" : "#222222"
			})
			
			$(".tr2 td").css({
				"background-color": "#323232"
			})
			
			getDataCnt();
			
		}, error : function (e1, e2, e3){
			console.log(e1, e2, e3)
		}
	});
};

var totLength;

function getDataCnt(){
	var url = ctxPath + "/getDataCnt.do";
	var param = "workDate=" + $("#today").val();
	
	totLength = 0;
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success : function(data){
			
			totLength = data;
						
			// console.log(totLength);
			$("#tg_cnt").html("&nbsp(" + totLength + ")");
			$("#tg_time").html("&nbsp(" + totLength + ")");
			
		}, error : function (e1, e2, e3){
			console.log(e1, e2, e3)
		}
	});
}

var handle = 0;
function time(){
	$("#time").html(getToday());
	 handle = requestAnimationFrame(time)
};

function setEl(){
	var neonColor = "#0096FF";
	
	var width = window.innerWidth;
	var height = window.innerHeight;
	
	$(".right").css({
		"height" : getElSize(120)
	});
	
	$(".left, .menu_left").css({
		"width" : getElSize(495)			
	})
	
	$("#container").css({
		"width" : contentWidth,
		"height" : contentHeight,
	});
	
	$("#container").css({
		"margin-left" : (originWidth/2) - ($("#container").width()/2),
		"margin-top" : (originHeight/2) - ($("#container").height()/2)
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.5,
		"position" : "absolute",
		//"background-color" : "black",
		"bottom" : 0 + marginHeight,
		"z-index" : 9998
	});
	
	$("#time").css({
		"color" : "white",
		"position" : "absolute",
		"font-size" : getElSize(40),
		"top" : getElSize(25) + marginHeight,
		"right" : getElSize(30) + marginWidth
	});
	
	$("#table").css({
		"position" : "absolute",
		"width" : $("#container").width(),
		"top" : getElSize(100) + marginHeight
	});
	
	$("#table2 td").css({
		"padding" : getElSize(20),
		"font-size": getElSize(40),
		"border": getElSize(5) + "px solid black"
	});
	
	
	$(".right").css({
		"width" : contentWidth - $(".left").width() 
	});
	
	$(".menu_right").css({
		"width" : $(".right").width()
	})
	
	$("#home").css({
		"cursor" : "pointer"
	})
	
	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"color" : "white",
		"font-size" : getElSize(40),
		"top" : getElSize(130) + marginHeight,
		"right" : getElSize(30) + marginWidth
	});
	
	$("span").css({
		"color" : "#8D8D8D",
		"position" : "absolute",
		"font-size" : getElSize(45),
		"margin-top" : getElSize(20),
		"margin-left" : getElSize(20)
	});
	
	$("#selected").css({
		"color" : "white",
	});
	
	$("span").parent("td").css({
		"cursor" : "pointer"
	});
	
	$(".title_span").css({
		"color" : "white",
		"font-size" : getElSize(40),
		"background-color" : "#353535",
		"padding" : getElSize(15)
	});
	
	
	$("select, input").css({
		"font-size" : getElSize(40),
//		"margin-left" : getElSize(20),
//		"margin-right" : getElSize(20)
	});
			
	$("button").css({
		"padding" : getElSize(15),
		"font-size" : getElSize(35),
	})
	
	$("#search").css({
		"cursor" : "pointer",
		"width" : getElSize(80),
	});
	
	$("#content_table td, #content_table2 td").css({
		"color" : "#BFBFBF",
		"font-size" : getElSize(50)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$("#delDiv").css({
		"position" : "absolute",
		"width" : getElSize(700),
		"height" :getElSize(200),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#delDiv").css({
		"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		//"z-index" : -1,
		"display" : "none",
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	$("#contentDiv").css({
		"overflow" : "auto",
		"width" : $(".menu_right").width()
	});
	
	$("#insertForm").css({
		"width" : getElSize(3000),
		"position" : "absolute",
		"z-index" : 999,
	});
	
	$("#insertForm").css({
		"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
		"top" : getElSize(400)
	});
	
	$("#insertForm table td").css({
		"font-size" : getElSize(70),
		"padding" : getElSize(15),
		"background-color" : "#323232"
	});
	
	$("#insertForm button, #insertForm select, #insertForm input").css({
		"font-size" : getElSize(60),
		"margin" : getElSize(15)
	});
	
	$(".table_title").css({
		"background-color" : "#222222",
		"color" : "white"
	});
	
	$("#contentTable td").css({
		"color" : "white",
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(100),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#app_store_iframe").css({
		"width" : getElSize(3100) + "px",
		"height" : getElSize(2000) + "px",
		"position" : "absolute",
		"z-index" : 9999,
		"display" : "block"
	});
	
	$("#app_store_iframe").css({
		"left" : originWidth * 1.5,
		"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
	}).attr("src", appServerUrl)
	
	
	$("#delDiv").css({
		"color" : "white",
		"z-index" : 9999999,
		"background-color" : "black",
		"position" : "absolute",
		"width" : getElSize(700) + "px",
		"font-size" : getElSize(60) + "px",
		"text-align" : "center",
		"padding" : getElSize(30) + "px",
		"border" : getElSize(7) + "px solid rgb(34,34,34)",
		"border-radius" : getElSize(50) + "px"
	});
	
	
	$("#delDiv div").css({
		"background-color" : "rgb(34,34,34)",
		"padding" : getElSize(20) + "px",
		"margin" : getElSize(10) + "px",
		"cursor" : "pointer"
	}).hover(function(){
		$(this).css({
			"background-color" : "white",
			"color" : "rgb(34,34,34)",
		})
	}, function(){
		$(this).css({
			"background-color" : "rgb(34,34,34)",
			"color" : "white",
		})
	});
	
	$("#delDiv").css({
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		"top" : - $("#delDiv").height() * 2
	});
	
	$("#delDiv div:nth(1)").click(function(){
		$("#delDiv").animate({
			"top" : - $("#delDiv").height() * 2
		});
	});
	
//	$("#save").css({
//		"float" : "left",
//		"font-size" : getElSize(50) + "px"
//	});
	
	$("#save").css({
		"float" : "left",
		"font-size" : getElSize(50),
		"background" : "linear-gradient(#bfbfbf, #9e9e9e)",
		"color" : "rgb(34,34,34)",
		"border" : "1px solid white",
		"margin-left" : getElSize(10),
		"border-radius" : getElSize(10),
		"margin-top" : getElSize(10),
		"width" : getElSize(200)
	}).hover(function(){
		$(this).css({
			"color" : "white",
		})
	}, function(){
		$(this).css({
			"color" : "rgb(34,34,34)",
		})
	});
	

	$("#tg_cnt").css({
		"color" : "white",
		"font-size" : getElSize(40),
		"margin-top" : getElSize(0),
		"margin-left" : getElSize(0)
	});
	
	$("#tg_time").css({
		"color" : "white",
		"font-size" : getElSize(40),
		"margin-top" : getElSize(0),
		"margin-left" : getElSize(0)
	});
	
}