<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>

<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	$(function(){
		setDivPos();
		
		$("#today").val(getToday().substr(0,10));
		
		$("#today").change(changeDateVal);
		$("#up").click(upDate);
		$("#down").click(downDate);
		
	});
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	
	function setDivPos(){
		
		
		// jane 변경
		/* $("#date_table").css({
			"position" : "absolute",
			"top" :getElSize(150),
			"right" : getElSize(100)
		}); */
		
		// jane 추가
		$("#date_table").css({
			"position" : "absolute",
			"top" :getElSize(300),
			"right" : getElSize(100)
		});
		
		$("#date_table button").css({
			"width" : getElSize(80),
			"height" : getElSize(80)
		});
		
		$("#up, #down").css({
			"width" : getElSize(70) *  1.2,
			"height" : getElSize(70)
		});
		
		$("#content_div").css({
			"margin-left" : getElSize(493) + "px"
		});
		
		$("#save_div").css({
			"margin-left" : getElSize(27) + "px"
		});
		
		if(getParameterByName('lang')=='ko'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		};
		
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	function upDate(){
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		changeDateVal();
	};
	
	function downDate(){
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		changeDateVal();
	};
	
	function changeDateVal(){
		window.sessionStorage.setItem("date", $("#today").val())		
		//window.sessionStorage.setItem("dvcId", $("#dvcId").val());
		
		var now = new Date(); 
		var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		
		var date = new Date();
		
		var year = date.getFullYear();
		var month = date.getMonth()+1;
		var day = date.getDate();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		// Set specificDate to a specified date at midnight.
		
		var selectedDate = $("#today").val();
		var s_year = selectedDate.substr(0,4);
		var s_month = selectedDate.substr(5,2);
		var s_day = selectedDate.substr(8,2);

		var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
		
		var time = year + "-" + month + "-" + day;
		
		var today = getToday().substr(0,10);
		
		//오후 6시 이후 날짜+1
		/* if(new Date().getHours() >= startHour){
			todayAtMidn.setDate(todayAtMidn.getDate() + 1)
			today = todayAtMidn.getFullYear() + "-" + addZero(String(todayAtMidn.getMonth()+1)) + "-" + addZero(String(todayAtMidn.getDate()))
		} */

		/* if(todayAtMidn.getTime()<specificDate.getTime()){
			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
			$("#today").val(today);
			return;
		}; */
		
		getData();
	};
	
	// 키 이벤트 값으로 판단
	function checkForNumber() {
	  var key = event.keyCode;
	  if(!(key==8||key==9||key==13||key==46||key==144||
	      (key>=48&&key<=57)||key==110||key==190)) {
	      event.returnValue = false;
	  }
	}
	 
	// 숫자만 입력과  특수문자('-','.',...)도 허용한다.
	function onlyNumber() {
	   if((event.keyCode > 31) && (event.keyCode < 45) || (event.keyCode > 57)) {
	      event.returnValue = false;
	   }
	}
	 
	// 숫자만 입력
	function onlyNumber2(loc) {
	   if(/[^0123456789]/g.test(loc.value)) {
	      alert("숫자가 아닙니다.\n\n0-9의 정수만 허용합니다.");
	      loc.value = "";
	      loc.focus();
	   }
	}
	
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/index.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
  	overflow-x : hidden;
}

</style> 

</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	
	<div id="time"></div>
	<div id="title"><spring:message code="add_prdct_target"></spring:message></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/config_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/sky_right.png" class='menu_right'>
					
					<table id="date_table">
						<tr>
							<td>
								<button id="up">▲︎</button><button id="down">▼</button>
							</td>
							<td>
								<input type="date" id="today" >		
							</td>
						</tr>
					</table>
					
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<center>
						<div style="width: 80%">
							<div style="float:left;" id="content_div">
								<table id="content_table" style="width: 100%; border-collapse: collapse;"> 
									<thead>
										<tr>
											<Td><spring:message code="device"/></Td>
											<%-- <Td><spring:message code="prdct_cnt"/></Td> --%>
											<!-- <Td>주야 구분</Td> -->
											<!-- <Td>목표 수량</Td> -->
											<Td><spring:message code="target_quantity"/> <strong id="tg_cnt" /></Td>
											<!-- <Td>싸이클당 생산량</Td> -->
											<!-- <Td>목표 가동시간</Td> -->
											<Td><spring:message code="target_optime"/> <strong id="tg_time" /></Td>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div style="float:left;" id="save_div">
								<%-- <button onclick="addTgCnt();" id="save"><spring:message code="save"></spring:message> </button> --%>
								<!-- jane 변경 --> 
								<button onclick="addTgCnt();" id="save"><spring:message code="save_button"></spring:message> </button>
							</div>
						</div>
					</center>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span" ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	